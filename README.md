# Latex CV

Just a nice easy way to format a CV, keeping everything where it should be and easy to update.

## Building locally

`docker run --rm -v %cd%:/cv -w /cv aergus/latex pdflatex -synctex=1 -interaction=nonstopmode -file-line-error -recorder -aux-directory=/cv/aux cv.tex`
