%-----------------------------------------------------------------------------------------------------------------------------------------------%
%	The MIT License (MIT)
%
%	Copyright (c) 2015 Jan Küster
%
%	Permission is hereby granted, free of charge, to any person obtaining a copy
%	of this software and associated documentation files (the "Software"), to deal
%	in the Software without restriction, including without limitation the rights
%	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%	copies of the Software, and to permit persons to whom the Software is
%	furnished to do so, subject to the following conditions:
%	
%	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%	THE SOFTWARE.
%	
%
%-----------------------------------------------------------------------------------------------------------------------------------------------%


%============================================================================%
%
%	DOCUMENT DEFINITION
%
%============================================================================%

%we use article class because we want to fully customize the page and don't use a cv template
\documentclass[11pt,A4]{article}	


%----------------------------------------------------------------------------------------
%	ENCODING
%----------------------------------------------------------------------------------------

%we use utf8 since we want to build from any machine
\usepackage[utf8]{inputenc}		

%----------------------------------------------------------------------------------------
%	LOGIC
%----------------------------------------------------------------------------------------

% provides \isempty test
\usepackage{xifthen}

%----------------------------------------------------------------------------------------
%	FONT
%----------------------------------------------------------------------------------------

% some tex-live fonts - choose your own

%\usepackage[defaultsans]{droidsans}
%\usepackage[default]{comfortaa}
%\usepackage{cmbright}
%\usepackage[default]{raleway}
%\usepackage{fetamont}
%\usepackage[default]{gillius}
%\usepackage[light,math]{iwona}
\usepackage[default]{roboto} 

% set font default
\renewcommand*\familydefault{\sfdefault} 	
\usepackage[T1]{fontenc}

% more font size definitions
\usepackage{moresize}		

\usepackage[none]{hyphenat}


%----------------------------------------------------------------------------------------
%	PAGE LAYOUT  DEFINITIONS
%----------------------------------------------------------------------------------------

%debug page outer frames
%\usepackage{showframe}			


%define page styles using geometry
\usepackage[a4paper]{geometry}		

%Add the full sized table package
\usepackage{tabularx}
\usepackage{colortbl}
\usepackage{multirow}

% for example, change the margins to 2 inches all round
\geometry{top=1.75cm, bottom=-.6cm, left=1.5cm, right=1.5cm} 	

%use customized header
\usepackage{fancyhdr}				
\pagestyle{fancy}

%less space between header and content
\setlength{\headheight}{-5pt}		

%indentation is zero
\setlength{\parindent}{0mm}

%----------------------------------------------------------------------------------------
%	TABLE /ARRAY DEFINITIONS
%---------------------------------------------------------------------------------------- 

%for layouting tables
\usepackage{multicol}			
\usepackage{multirow}

%extended aligning of tabular cells
\usepackage{array}

\newcolumntype{x}[1]{%
>{\raggedleft\hspace{0pt}}p{#1}}%


%----------------------------------------------------------------------------------------
%	GRAPHICS DEFINITIONS
%---------------------------------------------------------------------------------------- 

%for header image
\usepackage{graphicx}

%for floating figures
\usepackage{wrapfig}
\usepackage{float}
%\floatstyle{boxed} 
%\restylefloat{figure}

%for drawing graphics		
\usepackage{tikz}				
\usetikzlibrary{shapes, backgrounds,mindmap, trees}


%----------------------------------------------------------------------------------------
%	Color DEFINITIONS
%---------------------------------------------------------------------------------------- 

\usepackage{color}

%accent color
\definecolor{sectcol}{RGB}{255,150,0}

%dark background color
\definecolor{bgcol}{RGB}{110,110,110}

%light background / accent color
\definecolor{softcol}{RGB}{175,175,175}


%============================================================================%
%
%
%	DEFINITIONS
%
%
%============================================================================%

%----------------------------------------------------------------------------------------
% 	HEADER
%----------------------------------------------------------------------------------------

% remove top header line
\renewcommand{\headrulewidth}{0pt} 

%remove bottom header line
\renewcommand{\footrulewidth}{0pt}	  	

%remove pagenum
\renewcommand{\thepage}{}	

%remove section num		
\renewcommand{\thesection}{}			

%----------------------------------------------------------------------------------------
% 	ARROW GRAPHICS in Tikz
%----------------------------------------------------------------------------------------

% a six pointed arrow poiting to the right
\newcommand{\tzrarrow}{(0,0) -- (0.2,0) -- (0.3,0.2) -- (0.2,0.4) -- (0,0.4) -- (0.1,0.2) -- cycle;}	

% include the right arrow into a tikz picture
% param1: fill color
%
\newcommand{\rarrow}[1]
{\begin{tikzpicture}[scale=0.58]
	 \filldraw[fill=#1!100,draw=#1!100!black]  \tzrarrow
 \end{tikzpicture}
}


%----------------------------------------------------------------------------------------
%	custom sections
%----------------------------------------------------------------------------------------

% create a coloured box with arrow and title as cv section headline
% param 1: section title
%
\newcommand{\cvsection}[1]
{
    \colorbox{sectcol}{\mystrut \makebox[0.988\linewidth][l]{
        \rarrow{bgcol} \hspace{-7pt}  \rarrow{bgcol} \textcolor{white}{#1}
    }}
    \hspace{-3pt}
    \vspace{2pt}
}

%create a coloured arrow with title as cv meta section section
% param 1: meta section title
%
\newcommand{\metasection}[2]
{
    \begin{tabular*}{1\textwidth}{p{2.4cm} p{11cm}}
        \rarrow{bgcol}	\normalsize{\textcolor{sectcol}{#1}}&#2\\[12pt]
    \end{tabular*}
}


% Simple show a block of text
% param 1: Text to spread across the full page
%
\newcommand{\fitwidthtext}[1]
{
	\begin{tabular*}{1\textwidth}{p{17.6cm}}
        #1
	\end{tabular*}\\[-4pt]
}


% Fit 3 values across the page
% param 1: Text to spread across the full page
%
\newcommand{\infocolumns}[3]
{
	\begin{tabularx}{1\textwidth}{ 
        >{\raggedright\arraybackslash}X 
        | >{\centering\arraybackslash}X 
        | >{\raggedleft\arraybackslash}X }
        \textbf{#1} & \textbf{#2} & \textbf{#3}
	\end{tabularx}
}

%----------------------------------------------------------------------------------------
%	 CV EVENT
%----------------------------------------------------------------------------------------

% creates a stretched box as cv entry headline followed by two paragraphs about 
% the work you did
% param 1:	Date from (yyyy/mm)
% param 2:	Date to (yyyy/mm)
% param 3:	Position
% param 4:	techs used
% param 5:	institution (where did you work / study)
% param 6:	what was your position
%
\newcommand{\cvevent}[6]
{
    \vspace{6pt}
    \begin{tabularx}{1\textwidth}{p{1.8cm} p{11.6cm} p{3.4cm}}
        \arrayrulecolor{softcol}
        \multirow{2}{1.8cm}{\centering{#1}\\ \centering{-} \\\centering{#2}}
        & \textcolor{bgcol}{\textbf{#3}} \null\hfill {\textcolor{bgcol}{#4}}
        & \multicolumn{1}{|>{\raggedleft}X}{\textcolor{bgcol}{#5}} \\
        \cline{2-3}
        \noalign{\smallskip}
        \noalign{\smallskip}
        & \multicolumn{2}{>{\setlength\hsize{15.3cm}}X}{#6}
    \end{tabularx}
    \vspace{6pt}
}

% creates a stretched box as 
\newcommand{\cveventmeta}[2]
{
	\mbox{\mystrut \hspace{87pt}\textit{#1}}\\
	#2
}

%----------------------------------------------------------------------------------------
% CUSTOM STRUT FOR EMPTY BOXES
%----------------------------------------- -----------------------------------------------
\newcommand{\mystrut}{\rule[-0.26\baselineskip]{0pt}{\baselineskip}}


%============================================================================%
%
%
%
%	DOCUMENT CONTENT
%
%
%
%============================================================================%
\begin{document}


%use our custom fancy header definitions
\pagestyle{fancy}


%---------------------------------------------------------------------------------------
%	TITLE HEADLINE
%----------------------------------------------------------------------------------------


% Highlighted title
\hspace{0\linewidth}\colorbox{bgcol}{\makebox[0.984\linewidth][c]{\Huge{\textcolor{white}{\textsc{Joe Lewis Monk}} } } }

\vspace{6pt}

\infocolumns{joemonk.co.uk}{07757 017587}{joemonk@hotmail.co.uk}

%---------------------------------------------------------------------------------------
%	SUMMARY (optional)
%----------------------------------------------------------------------------------------

\vspace{-2pt}

\fitwidthtext{
  As a highly motivated and adaptive developer, my enthusiasm for learning new technologies along with years of rapid game and web development has driven my proficiency with many languages and tools, allowing me to be flexible when tackling problems. Over the last few years I have enjoyed expanding my role to include management of multiple large teams, and have picked up new tech stacks while moving between roles.


}

\vspace{-2pt}


%============================================================================%
%
%	CV SECTIONS AND EVENTS (MAIN CONTENT)
%
%============================================================================%

%---------------------------------------------------------------------------------------
%	EXPERIENCE
%---------------------------------------------------------------------------------------
\cvsection{Experience}


\cvevent{Present}{Jun 2022}{Senior Developer}{TS/JS/NodeJS/React}{Tes}{
  Changing fields into web development, I utilised my previous knowledge to pivot quickly into the technologies needed for full stack development.

  I have since worked on and improved many products across multiple teams, while using my experience to provide individual support within my team. I have also created internal initiatives to improve our developer experience as well as getting involved in higher level framework discussions to keep pushing the company forward.
}

\vspace{-12pt}
\textcolor{softcol}{\hrule}

\cvevent{Jun 2022}{Oct 2019}{Development Manager}{TS/JS/WebGL/NodeJS/Docker}{Live 5}{
  As development manager, I oversaw all of the developers at Live 5 and had a responsibility to oversee production of over 20 games a year from my teams. I kept each stage of game development on track to meet both internal and external deadlines. By implementing a proper code review process, frequent standups and additional tooling for both developers and artists, we produced far more complex games in less time with fewer bugs. In addition, I mentored both junior and senior members of my team to develop their technical skills and knowledge.

  While managing the team was my foremost responsibility, I was still heavily involved with development. I tackled any particularly difficult coding problems for the team and architected large-scale changes within the codebase. For example, I integrated new business vital services and rebuilt our base renderer and loading core in TypeScript.

  One of the more interesting projects I directed was to rebuild our backend, focusing on providing local and remote interfaces to the data generation that allowed for faster development of more reliable game backends. The deployment process was also rebuilt to allow deploying into AWS for browser game access, as a package for a separate serverless game build and to run a statistical analysis on a baremetal local kubernetes cluster which I also administered.
}

\vspace{-12pt}
\textcolor{softcol}{\hrule}

\cvevent{Oct 2019}{Sept 2018}{Game Developer}{TS/JS/WebGL}{Live 5}{
  I was hired to continue as a C++ Developer, but soon transitioned to the mobile team due to company priorities. Despite having no prior experience with JavaScript, I quickly became proficient in the language and its ecosystem, which enabled me to promptly integrate into my new role.

  I started with creating games, but similar to my time at Inspired, I wrote extra scripts and improved the game libraries to assist development across the team.
}

\vspace{-12pt}
\textcolor{softcol}{\hrule}

\cvevent{Sept 2018}{Mar 2016}{Engine/Game Developer}{C++/DirectX/Python}{Inspired Gaming}{
  My initial responsibilities involved converting existing games to work on a variety of hardware, though I quickly moved up to work on building some of the more complex games and tooling myself, before going on to mentor new starters.

  Soon after, I advanced into the game engine development team, which explored ways of improving the development cycle and coding efficiency for other developers. We improved the libraries, build steps and used middleware such as Conan and custom VS plugins provide prebuilt binaries and improve cohesion and standards across the teams.
}

\vspace{4pt}

\newpage

%---------------------------------------------------------------------------------------
%	EDUCATION SECTION
%--------------------------------------------------------------------------------------
\cvsection{Education}


\cvevent{2015}{2012}{First Class BSc. Honours in Computer Games Development}{}{UCLan}{
  My course revolved around creating games with C++ with DirectX, including modules on game and engine architecture, AI and game maths. For my large final year project, I built large scale, generated terrain, tessellated and procedurally textured purely in shaders.
}

\vspace{4pt}

\cvsection{Other Projects}


\fitwidthtext{
  External to my work life, I have a home server built around Docker Compose with Traefik and Authelia for routing and authentication to give me a fast secure base from which to work. This allows me to experiment with networking and hosted services such as private git hosting with continuous integration and single sign on throughout my applications.

  I have also used this along with wifi-enabled microcontrollers to explore automating parts of my house. This has provided their own interesting hardware and software problems including flashing custom firmware, dealing with communication protocols and providing a non-technical user interface to easily interact with the devices.

  During the summer I also build and fly FPV drones which have complex and intricate circuitry, wiring and configuration.
}


\vspace{4pt}

%============================================================================%
%
%
%
%	DOCUMENT END
%
%
%
%============================================================================%
\end{document}
